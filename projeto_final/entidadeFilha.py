from Entidades.entidadeMae import pessoa

class Cliente(pessoa):



  def __init__(self,identificador=0,nome="",idade=0,cpf=0,nacionalidade="",usuario=""):
    super().__init__(identificador,nome,idade,cpf,nacionalidade)
    self._usuario = usuario
    self._senha = 'Senha'

  def __str__(self):
    return '''
    ---cliente---
    identificador: {}
    nome: {}
    idade: {}
    CPF: {}
    nacinalidade: {}
    usuario: {}
    senha: {}
    '''.format(self.identificador,self.nome,self.idade,self.cpf,self.nacionalidade,self.usuario,self.senha)

  @property
  def usuario(self):
    return self._usuario

  @usuario.setter
  def usuario(self,usuario):
    self._usuario = usuario

  @property
  def senha(self):
    return self._senha

  @senha.setter
  def senha(self,senha):
    self._senha = senha



