from Entidades.entidadeFilha import Cliente
from Validador.validador import Validador


class Dados:

    def __init__(self):
        self._dados = dict()
        self.identificador = 0
 
    def buscarIdentificador(self,paramIdentificador):
        if(len(self._dados)== 0):
            print("Dicionario vazio")
        else:
            return self._dados.get(int(paramIdentificador))
    

    def buscarIdentificadorComLista(self,lista):
        print("Existem mutiplos registros com o filtro informado")
        for x in lista.values():
            print(x)
        print("Inserir um identificador:")
        return lista.get(int(Validador.verificarInteiro()))
     


    def buscarPorAtributo(self,param):
        lista = dict()
        if(len(self._dados)== 0):
            print("Dicionario vazio")
        else:
            for x in self._dados.values():
                if x.senha == param:
                    lista[x.identificador] = x
                    ultimoEncontrado = x.identificador
            if (len(lista) == 1):
                return lista[ultimoEncontrado]
            else:
               if (len(lista) == 0):
                print("Não encontrado")
                return None
            return self.buscarIdentificadorComLista(lista)
    
   

    def inserir(self,entidade):
        entidade.identificador = self.gerarProximoIdentificador()
        self._dados[entidade.identificador] = entidade
    
    def alterar(self,entidade):
        self._dados[entidade.identificador] = entidade

    
    def deletar(self,entidade):
        del(self._dados[entidade.identificador])

    def gerarProximoIdentificador(self):
        self.identificador = self.identificador + 1
        return self.identificador
    
    
    def criarCliente(self):
        p = Cliente()
        p.usuario = "Renata" 
        p.senha = "lancer123"
       

    
