#atribui valor a cebolas
cebolas = 300
#atribui valor a cebolas_na_caixa
cebolas_na_caixa = 120
#atribui valor a espaco_caixa
espaco_caixa = 5 
#atribui valor a caixas
caixas = 60
#calculo para obter cabolas_fora_da_caixa
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
#calculo para obter caixas_vazias
caixas_vazias = caixas - (cebolas_na_caixa / espaco_caixa)
#calculo para obter caixas_necessarias
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

#imprime quantas cebolas sao encaixotadas
print 'existem %s cebolas encaixotadas' % (cebolas_na_caixa)
#imprime quantas cebolas estao sem caixa
print 'existem %s cebolas sem caixa' % (cebolas_fora_da_caixa)
#imprime quantas cebolas cabem em cada caixa
print 'em cada caixa cabem %s cebolas' % (espaco_caixa)
#imprime quantas caixas estao vazias
print 'ainda temos %s caixas vazias' % (caixas_vazias)
#imprime quantas caixas sao necessarias
print 'entao, precisamos de %s caixas para empacotar todas as cebolas' % (caixas_necessarias)